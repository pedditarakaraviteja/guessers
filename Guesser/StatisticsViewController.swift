//
//  StatisticsViewController.swift
//  Guesser
//
//  Created by student on 2/27/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class StatisticsViewController: UIViewController {

    @IBOutlet weak var minLBL: UILabel!
    
    @IBOutlet weak var maxLBL: UILabel!
    
    @IBOutlet weak var mean: UILabel!
    
    @IBOutlet weak var stdivLBL: UILabel!
    
    
    
    
    
    @IBAction func clearStatistics(_ sender: Any) {
        minLBL.text = "0"
        maxLBL.text = "0"
        mean.text = "0.0"
        stdivLBL.text = "0.0"
        Guesser.shared.clearStatistics()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        minLBL.text = String(Guesser.shared.minimumNumAttempts())
        maxLBL.text = String(Guesser.shared.maximumNumAttempts())
        mean.text = String(format: "%.1f",Guesser.shared.mean())
        stdivLBL.text = String(format: "%.1f",Guesser.shared.stdDiv())
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        viewDidLoad()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
