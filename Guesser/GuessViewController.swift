//
//  FirstViewController.swift
//  Guesser
//
//  Created by student on 2/27/19.
//  Copyright © 2019 student. All rights reserved.
//

import UIKit

class GuessViewController: UIViewController {

    
    
    @IBOutlet weak var myGuessLBL: UILabel!
    
    @IBOutlet weak var guessTF: UITextField!
    
    @IBAction func AmIRightButton(_ sender: Any) {
        let guesss = Int(guessTF.text!)
        if guesss == nil{
            
        }else{
            let results = Guesser.shared.amIRight(guess: guesss!)
            if results == "Too Low" || results == "Too High"{
                myGuessLBL.text = results
            }else{
                displayMessage()
                Guesser.shared.createNewProblem()
                clear()
            }
            
        }
        
    }
    
    @IBAction func NewProblemButton(_ sender: Any) {
        Guesser.shared.createNewProblem()
    }
    
    
    func displayMessage(){
        let alert = UIAlertController(title: "Well done",message: "You got it in \(Guesser.shared.numAttempts) tries",preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default,handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func clear(){
        myGuessLBL.text = ""
        guessTF.text = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

